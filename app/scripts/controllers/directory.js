/*global angular*/
(function () {
    'use strict';

    /**
     * Returns url for entry relative to directory
     * @param {Object} directories Service
     * @param {String|Number} directoryId
     * @param {String|Number} entryId
     * @returns {String}
     */
    function getEntryUrl(directories, directoryId, entryId) {
        return directories.getEntryPathInsideDirectory(entryId, directoryId);
    }
    
    /**
     * @param {Object} $scope
     * @param {Object} directory
     */
    function processSuccessResponse($scope, directory) {
        $scope.directory = directory;
    }

    angular.module('sbertechnologiesApp').controller('DirectoryCtrl', [
        '$scope',
        'directories',
        '$routeParams',
        function ($scope, directories, $routeParams) {
            $scope.getEntryUrl = getEntryUrl.bind(null, directories, $routeParams.directory);

            $scope.directory = {};

            directories.getDirectory($routeParams.directory).then(processSuccessResponse.bind(null, $scope));
        }
    ]);
}());