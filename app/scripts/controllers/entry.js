/*global angular*/
(function () {
    'use strict';
    
    /**
     * @param   {Object}   entry
     * @returns {Object}
     */
    function normalizeEntry(entry) {
        var items = entry.items,
            i;
        
        entry.items = {};
        for (i = 0; i < items.length; i++) {
            entry.items[items[i].id] = items[i];
        }
        return entry;
    }
    
    /**
     * @param {Object}   $scope
     * @param {Object} entry
     */
    function processSuccessResponse($scope, entry) {
        $scope.entry = normalizeEntry(entry);
        $scope.defaultEntry = angular.copy($scope.entry);
    }
    
    /**
     * @param {Object} $scope
     */
    function reset($scope) {
        $scope.entry = angular.copy($scope.defaultEntry);
    }
    
    function emptyFn() {
        console.log('I don`t know what to do for this action');
    }
    
    angular.module('sbertechnologiesApp').controller('EntryCtrl', [
        '$scope',
        '$routeParams',
        'directories',
        function ($scope, $routeParams, directories) {
            $scope.entry = {};
            $scope.reset = reset.bind(null, $scope);
            $scope.remove = emptyFn.bind(null, $scope);
            $scope.save = emptyFn.bind(null, $scope);
            
            if ($routeParams.entry === 'new') {
                $scope.entry.items = $scope.entry.items || {
                    name: {},
                    weight: {},
                    point: {},
                    endDate: {},
                    describe: {},
                    units: {}
                };
                $scope.entry.items.name.name = 'Новый';
                
                $scope.defaultEntry = angular.copy($scope.entry);
            } else {
                directories.getEntry($routeParams.entry).then(processSuccessResponse.bind(null, $scope));
            }
        }
    ]);
}());
