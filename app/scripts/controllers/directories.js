/*global angular*/
(function () {
    'use strict';
    
    /**
     * @param {Object}   $scope
     * @param {Array.<Object>} directories
     */
    function processSuccessResponse($scope, directories) {
        $scope.directories = directories;
    }
    
    angular.module('sbertechnologiesApp').controller('DirectoriesCtrl', [
        '$scope',
        'directories',
        function ($scope, directories) {
            $scope.directories = [];

            directories.getDiectories().then(processSuccessResponse.bind(null, $scope));
        }
    ]);
}());