(function () {
    'use strict';
    
    var validators = {
        /**
         * @param {string}
         * @returns {boolean}
         */
        'INTEGER': function (modelValue) {
            var normalizedValue = modelValue.replace(/^0*/, ''),
                intValue = parseInt(normalizedValue, 10);
            if (typeof intValue !== 'number' || Number.isNaN(intValue) || intValue.toString() !== normalizedValue) {
                return false;
            }
            return true;
        },
        
        /**
         * @param {string}
         * @returns {boolean}
         */
        'FLOAT':  function (modelValue) {
            var normalizedValue = modelValue.toString().replace(/,/g, '.'),
                floatValue = parseFloat(normalizedValue);
            if (typeof floatValue !== 'number' || Number.isNaN(floatValue) || floatValue.toString() !== normalizedValue) {
                return false;
            }
            return true;
        }
    };

    /*global angular*/
    angular.module('sbertechnologiesApp').directive('validateByType', [
        function () {
            return {
                require: 'ngModel',
                link: function (scope, elm, attrs, ctrl) {
                    ctrl.$validators.byType = function (modelValue, viewValue) {
                        if (!attrs.validateByType || ctrl.$isEmpty(modelValue) || !validators[attrs.validateByType]) {
                            return true;
                        } else {
                            return validators[attrs.validateByType].call(null, modelValue, viewValue);
                        }
                    };
                }
            };
        }
    ]);
}());