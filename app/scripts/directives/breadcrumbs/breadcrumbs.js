/*global angular*/
angular.module('sbertechnologiesApp').directive('breadcrumbs', [
    'breadcrumbs',
    function (breadcrumbs) {
        'use strict';
        
        return {
            templateUrl: 'views/directives/breadcrumbs/breadcrumbs.html',
            link: function (scope) {
                scope.breadcrumbs = breadcrumbs.route;
            }
        };
    }
]);