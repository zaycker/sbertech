(function () {
    'use strict';
    
    var formatters = {
        /**
         * @param {string}
         * @returns {string}
         */
        'FLOAT':  function (value) {
            return value ? value.replace(/\./g, ',') : value;
        }
    };
    
    /**
     * @param   {string} value
     * @param   {string} type
     * @returns {string}
     */
    function formatValueByType(value, type) {
        return type && formatters[type] ? formatters[type](value) : value;
    }

    /*global angular*/
    angular.module('sbertechnologiesApp').directive('formatByType', [
        function () {
            return {
                require: 'ngModel',
                scope: {
                    formatByType: '='
                },
                link: function (scope, elm, attrs) {
                    scope.$watch('formatByType', function(newValue, oldValue) {
                        if (newValue) {
                            elm.val(formatValueByType(elm.val(), scope.formatByType));
                        }
                    });
                    elm.on('blur', function () {
                        if (!scope.formatByType) {
                            return;
                        }
                        elm.val(formatValueByType(elm.val(), scope.formatByType));
                    });
                }
            };
        }
    ]);
}());