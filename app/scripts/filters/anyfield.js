/*global angular*/
angular.module('sbertechnologiesApp').filter('anyfield', [function () {
  'use strict';

  function filterItem(pattern, fields, item) {
    var fieldsLength = fields.length,
        itemValue;
    pattern = pattern.toString().toLowerCase();
    for (var i = 0; i < fieldsLength; i++) {
      itemValue = item[fields[i].id].toString().toLowerCase();
      if (itemValue.indexOf(pattern) > -1) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param {Array.<Object>} filterItems
   * @param {String} pattern
   * @param {Array.<Object>} fields
   * @returns {Array.<Object>}
   */
  return function (filterItems, pattern, fields) {
    fields = fields || [];
    if (!pattern || !fields.length) {
      return filterItems;
    }

    return filterItems.filter(filterItem.bind(null, pattern, fields));
  };
}]);