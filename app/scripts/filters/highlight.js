/*global angular*/
angular.module('sbertechnologiesApp').filter('highlight', [function () {
    'use strict';
    
    var patternClass = 'highlight';
    
    function escapeRegExp (str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }
    
    return function (value, pattern, type) {
        if (!pattern) {
            return value;
        }
        
        if (type === 'FLOAT') {
            pattern = pattern.replace('.', ',');
        }
        
        return value.replace(new RegExp(escapeRegExp(pattern), 'gi'), '<span class="' + patternClass + '">$&</span>');
    };
}]);