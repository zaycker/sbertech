/*global angular*/
angular.module('sbertechnologiesApp').filter('cell', [function () {
    'use strict';
    
    /**
     * @param {Object} inputObject
     * @param {Object} fieldSpec
     * @returns {String} Cause it is used as Text node content
     */
    return function (inputObject, fieldSpec) {
        var value = inputObject.hasOwnProperty(fieldSpec.id) ? inputObject[fieldSpec.id] : '';
        
        switch (fieldSpec.type) {
            case 'INTEGER':
                return parseInt(value).toString();
            case 'FLOAT':
                return parseFloat(value).toString().replace('.', ',');
            default:
                return value.toString();
                
        }
    };
}]);