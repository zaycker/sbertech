/*global angular*/
angular.module('sbertechnologiesApp').factory('directories', [
  '$http',
  '$q',

  /**
   * The service is made cause of precaching mission.
   * @param   {Object} $http
   * @param   {Object} $q
   * @returns {Object}
   */
  function ($http, $q) {
        'use strict';

        var directories = [],
            directoriesDetails = {},
            entries = {};

        /**
         * Getter for list of ditrectories
         * @returns {Array.<Object>}
         */
        function getDiectories() {
            return $q(function (resolve) {
                if (directories.length) {
                    resolve(directories);
                } else {
                    $http.get('/data/getDirectories.json').then(function (response) {
                        directories = directories.concat(response.data.response.directories);
                        resolve(directories);
                    });
                }
            });
        }

        /**
         * Getter for directory
         * It sends directory id but json always responses with the same object.
         * @param   {Number} directoryId
         * @returns {Object}
         */
        function getDirectory(directoryId) {
            return $q(function (resolve) {
                if (directoriesDetails[directoryId]) {
                    resolve(directoriesDetails[directoryId]);
                } else {
                    $http.get('/data/getDirectory.json', {
                        params: {
                            id: directoryId
                        }
                    }).then(function (response) {
                        directoriesDetails[directoryId] = response.data.response.directory;
                        resolve(directoriesDetails[directoryId]);
                    });
                }
            });
        }

        /**
         * Getter for directory
         * It sends directory id but json always responses with the same object.
         * @param   {Number} directoryId
         * @returns {Object}
         */
        function getEntry(entryId) {
            return $q(function (resolve) {
                if (entries[entryId]) {
                    resolve(entries[entryId]);
                } else {
                    $http.get('/data/getEntry.json', {
                        params: {
                            id: entryId
                        }
                    }).then(function (response) {
                        entries[entryId] = response.data.response.entry;
                        resolve(entries[entryId]);
                    });
                }
            });
        }

        /**
         * @param {String|Number} entryId
         * @param {String|Number} directoryId
         * @returns {String}
         */
        function getEntryPathInsideDirectory(entryId, directoryId) {
            return getDirectoryPath(directoryId) + '/entry/' + entryId;
        }

        /**
         * @param   {String|Number} directoryId
         * @returns {String}
         */
        function getDirectoryPath(directoryId) {
            return '/#/directory/' + directoryId;
        }

        /**
         * @param   {String|Number} directoryId
         * @returns {String}
         */
        function getDirectoryName(directoryId) {
            for (var i = 0, l = directories.length; i < l; i++) {
                if (directories[i].id === directoryId) {
                    return directories[i].name;
                }
                return '';
            }
        }

        return {
            getDiectories: getDiectories,
            getDirectory: getDirectory,
            getEntry: getEntry,
            getDirectoryPath: getDirectoryPath,
            getDirectoryName: getDirectoryName,
            getEntryPathInsideDirectory: getEntryPathInsideDirectory,
        };
  }
]);