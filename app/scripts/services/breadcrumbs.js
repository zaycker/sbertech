/*global angular*/
angular.module('sbertechnologiesApp').factory('breadcrumbs', [
    '$rootScope',
    'directories',
    function ($rootScope, directories) {
      'use strict';
      
      var homeUrl = {
          title: 'Справочники',
          url: '/#/'
      };

      var route = [];

      function setDirectoryToRoute(directory) {
        route[1] = {
          title: directory.name,
          url: directories.getDirectoryPath(directory.id)
        };
      }
      
      $rootScope.$on('$routeChangeSuccess', function (event, current) {
        route.length = 0;
        if (!current.pathParams.directory) {
          return;
        }
        
        route.push(homeUrl);
        
        if (current.pathParams.entry) {
          route[1] = {};
          
          directories.getDirectory(current.pathParams.directory).then(setDirectoryToRoute);
        }
      });

      return {
        route: route
      };
    }
]);