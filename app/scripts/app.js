'use strict';

/**
 * @ngdoc overview
 * @name sbertechnologiesApp
 */
angular.module('sbertechnologiesApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angular-datepicker'
  ]).config(function ($routeProvider) {
    $routeProvider.when('/', {
      templateUrl: 'views/directories.html',
      controller: 'DirectoriesCtrl'
    }).when('/directory/:directory', {
      templateUrl: 'views/directory.html',
      controller: 'DirectoryCtrl'
    }).when('/directory/:directory/entry/:entry', {
      templateUrl: 'views/entry.html',
      controller: 'EntryCtrl'
    }).otherwise({
      redirectTo: '/'
    });
  });
