# sbertechnologies

## Notices

Cause  of format of definition of entry fields (array) it seems like fields are not determinate. That means that I can't show a form for "new entry" with some list of fields. I guess there has to be something like a button "add field" but form will be empty on init. But now it's made with assumption that Array is mistake. Fields have to be in an Object form with concrete determinate fieldnames if entry is not flex entity with variable fields.

There is no backend part that's why client always requests same file for directory or entry with `id` parameter but it always get the same response.

There is one test as an example in the project.

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.