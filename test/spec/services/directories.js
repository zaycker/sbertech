describe('directories service', function() {
    var directories;

    beforeEach(module('sbertechnologiesApp'));
    beforeEach(function () {
        inject(function ($injector) {
            directories = $injector.get('directories');
        });
    });

    it('should resolve right url of directory by id', function () {
        expect(directories.getDirectoryPath(3)).toBe('/#/directory/3');
    });
    it('should resolve right url of entity by ids or directory and entity', function () {
        expect(directories.getEntryPathInsideDirectory(2, 3)).toBe('/#/directory/3/entry/2');
    });
});